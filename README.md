# AdvancedJoinQuitMessage V1.1
プレイヤーがサーバーに入室・退室した時に表示されるメッセージをランダムに表示するPMMPプラグイン。

# 使い方
1. [ここからダウンロードして下さい。](https://forum.mcbe.jp/resources/511/download)
2. `[PMMPフォルダ]/plugins`フォルダにダウンロードしたPharを突っ込みます。
3. サーバーを起動すると、プラグインによって`[PMMPフォルダ]/plugin_data/AdvancedJoinQuitMessage`フォルダ内に空のjoinmessages.txtとquitmessages.txtが生成されます。
4. `[PMMPフォルダ]/plugin_data/AdvancedJoinQuitMessage`に移動し、joinmessages.txtとquitmessages.txtを編集します。
5. 以上で完了です。

# メッセージ編集
実際にメッセージをカスタムするには、`[PMMPフォルダ]/plugin_data/AdvancedJoinQuitMessage`フォルダ内のjoinmessages.txtとquitmessages.txtを編集する必要があります。これらのファイルは、一行につき一つのメッセージを追加できます。また、特殊文字を入れることも可能です(対応する特殊文字リストはこちら)。

joinmessages.txtとquitmessages.txtの役割は以下のとおりです。

- `joinmessages.txt` - プレイヤーがサーバーに入ったときに表示されるメッセージをカスタマイズします。
- `quitmessages.txt` - プレイヤーがサーバーから去った時に表示されるメッセージをカスタマイズします。

ファイルが空、もしくは空行のみの場合は、`§e%player が世界にやってきました(去りました)`を表示します。

# その他
このプラグインはMIT Licenseの元で提供されており、[GitLab](https://gitlab.com/masatoshiko/AdvancedJoinQuitMessage)でソースコードを閲覧することができます。
